require "rexml/document"
class NewsFilter < Nanoc::Filter
	identifier :news
	def run(content, params={})
		z = '<div id="newsheader"><span class="hide_mobile">Latest News</span><a href="#beginning">Read in Chronological Order</a><div style="clear:both;"></div></div>'
		doc = REXML::Document.new content
		doc.elements.each("statuses/status") { |s|
			if s.elements['favorite_count'].text.to_i > 0 then fav = true; else fav = false; end
			z+='<a name="headline_'+s.elements['id'].text+'" href="#headline_'+s.elements['id'].text+'">'+"\n"
			z+='<div class="item">'
			if fav then z+='<div class="favorite">'; end
			z+=s.elements['text'].text
			if fav then z+='</div>'; end
			z+='<div class="time">'+s.elements['created_at'].text+'</div>'+"\n"
			z+="</div></a>\n\n"
		}
		z+='<div id="newsfooter">OLDEST - <a href="#top">Back to Top</a></div><a name="beginning" />'
		z
	end
end
