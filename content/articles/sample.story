---
title: Scream Council Announces 2014 Terror Award Winners
date: 27 Jun 2014
author: Samothrace Exodus Watkinson
blurb: The Scream Council has announced the winners of the 2014 Terror Awards, with the Dread Cubes and the Pain Citadels again tying for first place.
---

<p>NEBRASKA FEAR PITS - The Scream Council has announced the winners of the hotly-contested 2014 Terror Awards. Speaking from the Dread Aerie, Screamlord Rafael Villa-Hernádez congratulated all "worthy contenders," but added that in the end, the Pain Citadels and the Dread Cubes remain "uncontested" in the "highly competitive" field of bowel-clenching terror.</p>

<p>"The Dread Cubes demonstrated particular excellence in their innovative and exceptionally cruel assault on Miami this year, but the sheer industrial scale of the Pain Citadels puts them in a league of their own. Ultimately, we couldn't justify awarding one over the other, and elected to award first place to both parties."</p>

<p>Interviewed at his dread halls in the Washington Pain Citadel, the Pain King was reportedly "delighted" with the outcome, indicating he will personally travel to the ceremony to collect the award on behalf of his organization. "This is an inspiring testimony to the hard work and dedication of the thousands of electricians, fleshreavers, acid specialists, and mortifactors who make the Pain Citadels the sprawling catacombs of unrelenting horror we've all come to know and love," said the Pain King, adding with an "insouciant" wink, "but maybe next year we'll finally beat those silly Dread Cubes."</p>

<p>All reporters present were then savagely beaten and dragged away to the mortification chambers, where they are expected to remain for the rest of their natural lives.</p>

<p>The Dread Cubes could not be reached for comment, and are not expected to attend the ceremony, as cube scientists remain uncertain whether they are sentient. The commissioner of the Florida State Police is scheduled to accept the award on the Dread Cubes' behalf.</p>

<p>Pressed on the apparent exclusion of the Queenspawn from the awards, Villa-Hernández explained that the Scream Council "neither supports nor condones bone heresy," and additionally warned that all Contaminated contestants would be "automatically disqualified." Recently deposed Russian President Vladimir Putin, currently awaiting trial for war crimes and treason against humanity, dismissed the Scream Council's justifications, claiming the Queenspawn exclusion was a result of "bias," and noting that infamous bone heretic Dark Lord Nixon placed first in the Terror Awards three times during the Reign of Blood.</p>

<p>Russian Supreme Leader Anastasiya Vasilyeva apologized Thursday for Putin's comments, adding  "It is unclear how he keeps regrowing his tongue."</p>

<p><small><i>Samothrace Exodus Watkinson, Esq. is Approved News 6's entertainment correspondent, boasting an unvarying 374 years of industry experience due to an childhood chronomancy accident. Xe lives in New York City with hir massive personal harem, seventeen gerbils, and extensive collection of blackmail material.</i></small></p>
