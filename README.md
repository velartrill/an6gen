# README #

an6gen is the backend used to manage the official, Approved website of [Approved News 6](http://velartrill.github.io/approvednews/). ([Approved News 5](http://velartrill.github.io/approvednews5) should be considered Contaminated.)

## History ##

The first incarnation of the Approved News 6 site was just a handful of manually-maintained HTML pages and an XSLT stylesheet to format the output of the twitter archive. This was already unwieldy when I started out, and when I got to needing to add full articles, it became completely unfeasible, so I went in search of a static site generator instead.

Originally, I was going to base an6gen on Jekyll. As it turned out tho, Jekyll wanted to install a metric fuckton of dependencies and the refused to run without node.js, at which point I lost patience and went to look for a less crappy alternative. I found [nanoc](http://nanoc.ws/).

First, the bad: Using nanoc is not as smooth as Jekyll. While it's more flexible, it takes more work on your end to get things up and running. nanoc's documentation is also nearly worthless, and some of its userbase weirdly hostile. At one point I was trying to disable the (frankly ludicrous) default of "pretty urls," which is nanocese for "create an entire directory for every single page just so the end user won't see those icky file extensions, and it took me hours of frustration to figure out how. I found exactly one person who had asked how to disable this scheme in the past, and they were immediately shouted at by other nanoc users for heresy. Eventually I figured out it required changes to the routing rules, and another frustrated half hour trying to work out the byzantine incantations needed to make nanoc cooperate. I did finally get it working, tho.

The good: once you work out how to use it, nanoc is very flexible and unlike CERTAIN frameworks I could mention, is not needlessly complicated. Adding new page types and filters was so straightforward I was certain I was doing something wrong. Its dependencies are negligible.

## Components ##

an6gen uses nanoc for site generation, and [TwitterBackup](http://johannburkard.de/blog/programming/java/backup-twitter-tweets-with-twitterbackup.html) (a.k.a. the ONLY remaining tweet-downloader that still works) for downloading an archive of tweets. Small caveat: Twitter changed their API recently in a way that broke no-longer-maintained TwitterBackup and I had to manually patch the JAR to get it running again.

I'm not sharing my binary because it's probably not legal to do that without the source, and I'm hesitant to share a bunch of source code I don't understand (I couldn't even tell you how to build the whole thing) and have no interest in maintaining. If someone wants me to, I might be able to track down the troublesome class and publish my fix as a diff file or something (it was just a matter of adding a field; TwitterBackup makes unwise use of reflection so what should have been a minor change to the Twitter API completely broke the program). But unless there's someone else who wants to use this system, I'm not going to go to the trouble.

It has no other dependencies, relying entirely on builtin Ruby libraries for parsing XML.

## File Types ##

an6gen recognizes three types of file. HTML files are slotted into the default template, but no other formatting is applied. Their metadata controls their navbar linkage, CSS, and title. `.story` files contain news stories and are listed under the "Articles" page. `.news` files are TwitterBackup-produced XML files which are parsed to create a readable chronological archive. Their metadata is best stored in the corresponding `.yaml` file.

## Build Process ##

1. `cd` to the root directory of the project.
2. Invoke `nanoc`.
3. Barring errors, the output will be in the `approvednews` subdirectory.

## LICENSE ##

All the Ruby code I wrote and the underlying architecture of the site I'm releasing under the GPLv2. I reserve all rights to the sample images, design, and text content included.